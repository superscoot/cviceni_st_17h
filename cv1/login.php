<!DOCTYPE html>

<html>
  <head>
      <title>My first web</title>
      <link rel="stylesheet" type="text/css" href="style.css"> 
  </head>
  
  <body>
    <header></header>
    <main>
        <nav>
            <ul> <!-- unordered list -->
                <li> <a href="#">Menu 1</a> </li>
                <li> <a href="#">Menu 2</a> </li>
                <li> <a href="#">Menu 3</a> </li>
                <li> <a href="login.php">Login</a> </li>
            </ul>
        </nav>

        <section class="form">
        	<form method="post">
        		<label>Username
        			<input type="text" name="username" required>
        		</label>
        		<br>
        		<label>Password
        			<input type="password" name="password" required>
        		</label>
        		<br>
        		<label>Planet
        			<select name="planet">
        				<option value="earth">Earth</option>
        				<option value="Betelgeuse">Betelgeuse</option>
        				<option value="Proxima B">Proxima B</option>
        			</select>
        		</label>
        		<br>
        		<input type="submit" value="Send">
        	</form>
        </section>

        <?php
        	if (!empty($_POST['username'])) {
        		$users = file_get_contents("https://akela.mendelu.cz/~xvalovic/mock_users.json"); # Stahne subor zo zdroja
        		$users_json = json_decode($users); # Dekoduje z json

        		print var_dump($users_json); # kontrolny vypis

        		print "
        			<h1> Welecome " . htmlspecialchars($_POST['username']) . "</h1>
        			<p>visitor from {$_POST['planet']}!</p>
        		";
        	}
        ?>

</main>
</body>
</html>
